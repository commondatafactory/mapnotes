package main

import "gitlab.com/commondatafactory/mapnotes/routers"

func main() {
	routers.Start()
}
