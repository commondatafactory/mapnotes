# alternative solution to start API

# source ./alternative-local-dev.sh
# check the variables using $ env
# Run the database through $ docker compose
# then run $ make watch 
# This starts mapnotes and tiles

export POSTGRES_DB=dev
export POSTGRES_HOST=127.0.0.1
export POSTGRES_PORT=5434
export POSTGRES_PASSWORD=insecure
export SSLMODE=disable

