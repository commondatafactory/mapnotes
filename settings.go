package mapnotes

import (
	"github.com/Attumm/settingo/settingo"
)

func DefaultSettings() {
	settingo.Set("http_host", "0.0.0.0:8000", "host with port")
	settingo.Set("CORS", "y", "CORS enabled")

	settingo.SetBool("debug", true, "debug information during run")

	settingo.Set("POSTGRES_DB", "dev", "database name")
	settingo.Set("POSTGRES_HOST", "127.0.0.1", "database host")
	settingo.Set("POSTGRES_PORT", "5432", "database port")
	settingo.Set("POSTGRES_USER", "cdf", "database user")
	settingo.Set("POSTGRES_PASSWORD", "insecure", "database password")
	settingo.Set("SSLMODE", "require", "sslmode require / disable")

	settingo.SetBool("status", true, "print some database statistics")
	settingo.SetBool("randomdata", false, "fill database with random nl data")
}

func TestSettings() {
	DefaultSettings()
	settingo.Set("POSTGRES_DB", "test", "database name")
	settingo.Set("SSLMODE", "disable", "sslmode require / disable")
	settingo.Set("POSTGRES_PORT", "5434", "database name")
	//settingo.Set("POSTGRES_HOST", "database", "database name")
}
