package routers

import (
	"fmt"
	"log"
	"os"

	"github.com/Attumm/settingo/settingo"

	"github.com/gofiber/fiber/v2"

	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/commondatafactory/mapnotes"
	"gitlab.com/commondatafactory/mapnotes/models"
)

func setupRoutes(app *fiber.App) *fiber.App {
	app.Get("/", HelpRest)
	app.Get("/v1/", HelpRest)
	app.Get("/status", func(c *fiber.Ctx) error {
		return c.SendString(models.StatusLine())
	})

	app.Get("/help", HelpRest)

	finding := app.Group("/findings")
	finding.Get("/", ListFindings)

	finding.Delete(":id", DeleteFinding)
	finding.Get(":id/", ListFindings)

	finding.Put("/:id", UpdateFinding)
	finding.Post("/", CreateFinding)

	finding.Post("/:id/notes", CreateNote)
	finding.Get("/:id/notes", ListNotes)
	finding.Get("/:id/notes/:noteID", ListNotes)
	finding.Delete("/:id/notes/:noteID", DeleteNote)

	return app
}

func Start() {
	fmt.Println("starting mapnotes api..")

	app := fiber.New(
		fiber.Config{
			ServerHeader: "Commondatafactory Mapnotes",
			AppName:      "Mapnotes App v0.0.1",
		})
	

	// NOTE: CORS for when testing the API directly and locally 
	app.Use(cors.New(cors.Config{
		AllowOrigins: "http://127.0.0.1:3000, http://localhost:3000, https://*.commondatafactory.nl",
		AllowHeaders:  "Origin, Content-Type, Accept",
		AllowCredentials: true,
		AllowMethods: "GET,POST,HEAD,PUT,DELETE,PATCH,OPTIONS",
	}))

	app = setupRoutes(app)
	cf := logger.ConfigDefault
	cf.Output = os.Stdout

	app.Use(logger.New(cf))
	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404) // => 404 "Not Found"
	})

	fmt.Println("setting up settings")
	mapnotes.DefaultSettings()
	settingo.Parse()

	fmt.Println("setting up db")

	err := models.SetupDB()

	if err != nil {
		fmt.Println("database setup failed")
	}

	if settingo.GetBool("randomdata") {
		models.MakeRandomFindings(2000)
	}

	fmt.Println("setting up api..")

	IPPort := settingo.Get("http_host")

	fmt.Printf("starting..on %s", IPPort)
	log.Fatal(app.Listen(IPPort))
}
