package routers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/geojson"
	"gitlab.com/commondatafactory/mapnotes"
	"gitlab.com/commondatafactory/mapnotes/models"
)

var app *fiber.App

func init() {
	mapnotes.TestSettings()

	if err := models.SetupDB(); err != nil {
		log.Fatal(err)
	}

	app = fiber.New(
		fiber.Config{
			ServerHeader: "TEST Commondatafactory Mapnotes",
			AppName:      "TEST",
		})

	app = setupRoutes(app)
}

func _parseBody(resp *http.Response, t *testing.T) map[string]interface{} {

	var unknown map[string]interface{}

	b, _ := io.ReadAll(resp.Body)
	err := json.Unmarshal(b, &unknown)

	if err != nil {
		t.Error(err)
	}

	resp.Body.Close()

	return unknown
}

func _parseList(resp *http.Response, t *testing.T) []map[string]interface{} {
	var unknown []map[string]interface{}

	b, _ := io.ReadAll(resp.Body)
	err := json.Unmarshal(b, &unknown)

	if err != nil {
		t.Error(err)
	}

	resp.Body.Close()

	return unknown
}

func postFindingPolygon() (*http.Response, error) {
	data := url.Values{}

	geoJSON := `{
	"type": "Polygon",
	"coordinates": [
		[
		    [4.905321, 52.377706],
		    [4.90527, 52.377706],
		    [4.90527, 52.377869],
		    [4.905321, 52.377869],
		    [4.905321, 52.377706]
		]
	]
}
	`

	data.Set("geojson", geoJSON)
	data.Set("reporter", "john doo")
	data.Set("street", "sesamstraat")
	data.Set("streetnumber", "2")
	data.Set("postalcode", "1234AB")

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/findings/", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	return app.Test(req)
}

func TestDeleteFinding(t *testing.T) {
	models.TruncateTables()
	// post a finding.
	resp, err := postFindingPolygon()

	if err != nil {
		t.Error(err)
		return
	}

	unknown := _parseBody(resp, t)

	fmt.Println(unknown["data"])

	data := url.Values{}
	params := strings.NewReader(data.Encode())
	req := httptest.NewRequest("GET", "/findings/", params)

	resp, _ = app.Test(req)
	unknownList := _parseList(resp, t)

	if len(unknownList) != 1 {
		t.Error(errors.New("should be one present."))
	}

	fmt.Println(unknownList)
	fIDi := unknownList[0]["ID"]
	fmt.Println(unknownList[0]["ID"])
	fID := fmt.Sprint(fIDi)
	data = url.Values{}
	params = strings.NewReader(data.Encode())
	req = httptest.NewRequest("DELETE", "/findings/"+fID, params)

	resp, _ = app.Test(req)

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusNoContent {
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
		t.Error("wrong status code", resp.StatusCode)
	}

	// check if there now 0 items.
	params = strings.NewReader(data.Encode())
	req = httptest.NewRequest("GET", "/findings/", params)

	resp, _ = app.Test(req)
	unknownList = _parseList(resp, t)

	if len(unknownList) != 0 {
		t.Error(errors.New("should be zero present."))
	}
}

func TestPolygonPost(t *testing.T) {

	models.TruncateTables()

	resp, err := postFindingPolygon()

	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}
}

func TestStoreFindingInDB(t *testing.T) {

	models.TruncateTables()

	geoJSON := `
{
	"type": "Polygon",
	"coordinates": [
		[
		    [4.905321, 52.377706],
		    [4.90527, 52.377706],
		    [4.90527, 52.377869],
		    [4.905321, 52.377869],
		    [4.905321, 52.377706]
		]
	]
}
	`

	finding := models.Finding{}
	finding.Reporter = "test reporter"

	var g geom.T
	if err := geojson.Unmarshal([]byte(geoJSON), &g); err != nil {
		t.Error(err)
	}

	gc := geom.NewGeometryCollection()
	if err := gc.Push(g); err != nil {
		t.Error(err)
	}

	finding.Geom = &models.Geometry{Geom: gc}

	if err := models.DB.Create(&finding).Error; err != nil {
		t.Error(err)
	}
}

func _postPointFinding(t *testing.T) *http.Response {
	data := url.Values{}
	geoJSON := `
{
	"type": "Point",
	"coordinates": [4.906321, 52.427706]
}
	`
	// 52.426993/4.90643
	data.Set("geojson", geoJSON)
	data.Set("reporter", "john doo")
	data.Set("street", "sesamstraat")
	data.Set("street_number", "2")
	data.Set("category", "bla")

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/findings/", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := app.Test(req)

	if err != nil {
		t.Error(err)
	}

	return resp
}

func TestFindingPointPost(t *testing.T) {

	models.TruncateTables()

	resp := _postPointFinding(t)

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}
}

func TestUpdateFinding(t *testing.T) {
	var err error

	models.TruncateTables()

	resp := _postPointFinding(t)
	unknown := _parseBody(resp, t)

	findingdata := unknown["data"]
	fd := findingdata.(map[string]interface{})
	findingID := fmt.Sprint(fd["ID"])
	resp.Body.Close()

	data := url.Values{}
	data.Set("title", "updatedone")

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("PUT", "/findings/"+findingID, params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err = app.Test(req)
	if err != nil {
		t.Error(err)
	}

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}

	data = url.Values{}
	params = strings.NewReader(data.Encode())
	req = httptest.NewRequest("GET", "/findings/"+findingID, params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp.Body.Close()

	resp, _ = app.Test(req)

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}

	b, _ := io.ReadAll(resp.Body)
	resp.Body.Close()

	var findingList []map[string]interface{}

	err = json.Unmarshal(b, &findingList)
	if err != nil {
		t.Error(err)
	}

	if len(findingList) != 1 {
		t.Error(errors.New("finding seems missing"))
	}

	findingdata = findingList[0]
	fd = findingdata.(map[string]interface{})

	title := fmt.Sprint(fd["title"])

	if title != "updatedone" {
		t.Error("update failed")
	}

	category := fmt.Sprint(fd["category"])

	if category != "bla" {
		t.Errorf("category %s !=bla should not have changed", category)
	}
}

func TestListFindings(t *testing.T) {
	// put at least one thing in database
	models.TruncateTables()

	resp, err := postFindingPolygon()

	if err != nil {
		t.Error(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}

	// now test the listing

	data := url.Values{}
	params := strings.NewReader(data.Encode())
	req := httptest.NewRequest("GET", "/findings/", params)

	resp, err = app.Test(req)

	if err != nil {
		t.Error(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}

	b, _ := io.ReadAll(resp.Body)

	var unknown []map[string]interface{}

	err = json.Unmarshal(b, &unknown)
	if err != nil {
		t.Error(err)
	}

	if len(unknown) != 1 {
		t.Error(errors.New("geojson seems missing"))
	}
}

func TestPostNote(t *testing.T) {

	models.TruncateTables()

	resp, err := postFindingPolygon()

	if err != nil {
		t.Error(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))

		return
	}

	b, _ := io.ReadAll(resp.Body)

	var unknown map[string]interface{}

	err = json.Unmarshal(b, &unknown)
	if err != nil {
		t.Error(err)
	}

	findingdata := unknown["data"]

	fd := findingdata.(map[string]interface{})

	findingID := fmt.Sprint(fd["ID"])
	data := url.Values{}
	data.Set("finding_id", findingID)
	data.Set("title", "onregelmatige bedoeling")
	data.Set("reporter", "john doo")
	data.Set("image", "doesnotworkyet")

	params := strings.NewReader(data.Encode())

	req := httptest.NewRequest("POST", "/findings/"+findingID+"/notes", params)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err = app.Test(req)
	if err != nil {
		t.Error(err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != fiber.StatusOK {
		t.Error("wrong status code", resp.StatusCode)
		b, _ := io.ReadAll(resp.Body)
		t.Error(string(b))
	}
}
