package routers

import (
	"fmt"
	"log"
	"strconv"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/commondatafactory/mapnotes/models"
)

type HelpStruct struct {
	ListFindings string
	PostFinding  string
	PostNote     string
	Help         string
}

func HelpRest(c *fiber.Ctx) error {

	host := c.BaseURL()

	response := HelpStruct{
		ListFindings: fmt.Sprintf("%s/findings/", host),
		PostFinding:  fmt.Sprintf("%s/finding/", host),
		PostNote:     fmt.Sprintf("%s/finding/:id/note/", host),
		Help:         fmt.Sprintf("%s/help/", host),
	}

	return c.JSON(response)
}

func limits(c *fiber.Ctx) (limit, offset int) {
	limit = 500
	offset = -1

	limitStr := c.FormValue("limit")
	offsetStr := c.FormValue("offset")

	if limitStr != "" {
		limit, _ = strconv.Atoi(limitStr)
	}

	if offsetStr != "" {
		offset, _ = strconv.Atoi(offsetStr)
	}

	return limit, offset
}

func ListFindings(c *fiber.Ctx) error {
	findingID := c.Params("id")
	findings := []models.Finding{}
	limit, offset := limits(c)

	if findingID != "" {
		models.DB.Where("id = ?", findingID).Find(&findings)
	} else {
		models.DB.Where("id > ?", offset).Order("created_at desc").Find(&findings).Limit(limit)
	}

	if len(findings) == 0 {
		return c.Status(fiber.StatusNotFound).JSON(findings)
	}

	return c.Status(fiber.StatusOK).JSON(findings)
}

func CreateFinding(c *fiber.Ctx) error {
	finding := models.Finding{}

	// Store the body in finding
	err := c.BodyParser(&finding)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"status": "error", "message": err, "data": err})
	}

	err = models.DB.Create(&finding).Error

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"status":  "error",
			"message": "Could not create finding",
			"data":    err.Error()})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "Created Finding", "data": finding})
}

func ListNotes(c *fiber.Ctx) error {
	limit, offset := limits(c)
	noteID := c.Params("noteid")
	notes := []models.Note{}

	if noteID != "" {
		models.DB.Where("id = ?", noteID).Find(&notes)
	} else {
		models.DB.Where("id > ?", offset).Order("created_at desc").Find(&notes).Limit(limit)
	}

	if len(notes) == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"status":  "error",
			"message": "Could not find note"})
	}

	return c.Status(fiber.StatusOK).JSON(notes)
}

// CreateNote and let it point to a Finding.
func CreateNote(c *fiber.Ctx) error {
	note := models.Note{}
	// Store the body in finding
	err := c.BodyParser(&note)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"status": "error", "message": err, "data": err})
	}

	findingID := c.Params("id")

	if findingID == "" {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"status": "error", "message": "Finding ID empty", "data": err})
	}

	fid, _ := strconv.Atoi(findingID)
	findings := []models.Finding{}
	models.DB.Where("ID = ?", fid).Find(&findings)

	if len(findings) == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "Finding doest not exists", "data": err})
	}

	note.FindingID = findingID
	err = models.DB.Create(&note).Error

	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "Could not create note", "data": err})
	}

	return c.JSON(fiber.Map{"status": "success", "message": "Created Note", "data": note})
}

type findingObject struct {
	Title          string `json:"title"`
	Description    string `json:"description"`
	PandID         string `json:"pandId"`
	Street         string `json:"street"`
	StreetNumber   int    `json:"streetNumber"`
	AddresDisplay  string `json:"addressDisplay"`
	AdditionLetter string `json:"additionLetter"`
	Addition       string `json:"addition"`
	PostalCode     string `json:"postalCode"`
	// Municipality   *string          `json:"municipality"`
	// Details        *datatypes.JSON  `json:"details"`
	// Geojson        *models.Geometry `json:"geojson"`
	// GeoData        *datatypes.JSON  `json:"geoData"`
	// Organization   *string          `json:"organization"`
	Category string `json:"category" form:"category"`
}

func (u *findingObject) Into(f *models.Finding) {
	minLenght := 2

	if len(u.Title) > minLenght {
		f.Title = u.Title
	}

	if len(u.Description) > minLenght {
		f.Description = u.Description
	}

	if len(u.Category) > minLenght {
		f.Category = u.Category
	}
}

func UpdateFinding(c *fiber.Ctx) error {
	var finding models.Finding

	// Read the param id
	id := c.Params("id")

	// Find the finding with the given Id
	models.DB.Find(&finding, "id = ?", id)

	// If no such finding present return an error
	if finding.ID == 0 {
		log.Println("no finding id?")
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No fiding present", "data": nil})
	}

	// Store the body containing the updated data and return error if encountered
	var updateFindingGiven findingObject

	err := c.BodyParser(&updateFindingGiven)

	if err != nil {
		log.Println("oopsie")
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}

	updateFindingGiven.Into(&finding)

	// Save the Changes
	models.DB.Save(&finding)

	// Return the updated finding
	return c.Status(fiber.StatusOK).JSON(fiber.Map{"status": "success", "message": "Updated finding", "data": finding})
}

func DeleteFinding(c *fiber.Ctx) error {
	var (
		finding models.Finding
		notes   []models.Note
		idx     int64
		err     error
	)

	// Read the param id
	id := c.Params("id")

	baseTen := 10
	if idx, err = strconv.ParseInt(id, baseTen, 0); err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No finding ID present", "data": nil})
	}

	// Find the finding with the given Id
	models.DB.Find(&finding, "id = ?", idx)

	// If no such finding present return an error
	if finding.ID == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No finding present", "data": nil})
	}

	// Delete Finding related data
	models.DB.Delete(&finding)
	models.DB.Where("finding_id = ?", id).Delete(&notes)

	// Return the updated finding (204)
	return c.Status(fiber.StatusNoContent).JSON(fiber.Map{"status": "success", "message": "Deleted Finding", "data": finding})
}

func DeleteNote(c *fiber.Ctx) error {
	var (
		finding models.Finding
		note    models.Note
		idx     int64
		noteID  int64
		err     error
	)

	// Read the param id
	id := c.Params("id")
	nid := c.Params("noteID")

	baseTen := 10
	if idx, err = strconv.ParseInt(id, baseTen, 0); err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No finding ID present", "data": nil})
	}

	if noteID, err = strconv.ParseInt(nid, baseTen, 0); err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No Note ID present", "data": nil})
	}

	// Find the finding with the given Id
	models.DB.Find(&finding, "id = ?", idx)

	// If no such finding present return an error
	if finding.ID == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No finding present", "data": nil})
	}

	models.DB.Find(&note, "id = ?", noteID)

	if note.ID == 0 {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"status": "error", "message": "No note found for given ID", "data": nil})
	}

	models.DB.Delete(note)

	// Return the updated finding (204)
	return c.Status(fiber.StatusNoContent).JSON(fiber.Map{"status": "success", "message": "Deleted Finding", "data": note})
}
