# STEP 1 build binary
FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git
RUN apk --no-cache add ca-certificates

WORKDIR /app
COPY cmd  /app/cmd
COPY models /app/models
COPY routers /app/routers

COPY *.go /app/
COPY go.* /app/

# Fetch dependencies.
RUN go get -d -v

# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build -o cmd/api cmd/main.go

# STEP 2 build a small image
FROM scratch

# Copy static executable and certificates
COPY --from=builder /app/cmd/api /app/cmd/api
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

WORKDIR /app

ENTRYPOINT ["/app/cmd/api"]
