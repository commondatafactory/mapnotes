# MAPNOTES

Mapnotes is an API service to register notes/findings on a map.

Register geometric- and metadata for various findings.

- Accepts polygons, lines and points
- Saves the data to a POSTGIS database
- Seperates notes from findings

# STATE

This project is under construction.

Known issues:

- The `Makefile` is under construction
- Findings start with id `1` rather than `0`

# RUN LOCALLY

Update the packages if needed

```sh
go get -u ./...
```

Spin up the database

```sh
docker-compose up database
```

Then start either either the API, the tileserver or both

```sh
docker-compose build --no-cache mapnotes
docker-compose up mapnotes
docker-compose up tiles
```

# BUILD

Run the `./build_push.sh` file in the terminal and include a name like so:

```sh
./build_push.sh new-version
```
