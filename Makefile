build:
	go build -o cmd/api cmd/main.go

run: build
	./cmd/api

watch:
	reflex -s -r '\.go$$' make run
