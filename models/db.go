package models

import (
	"fmt"

	"log"

	"github.com/Attumm/settingo/settingo"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var DB *gorm.DB

func SetupDB() error {

	dsn := fmt.Sprintf(
		`host=%v user=%v password=%v dbname=%v port=%v sslmode=%v`,
		settingo.Get("POSTGRES_HOST"),
		settingo.Get("POSTGRES_USER"),
		settingo.Get("POSTGRES_PASSWORD"),
		settingo.Get("POSTGRES_DB"),
		settingo.Get("POSTGRES_PORT"),
		settingo.Get("SSLMODE"),
	)

	var err error

	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Error),
	})

	if err != nil {
		log.Fatal(err)
	}

	err = customMigrateTables()

	if err != nil {
		log.Fatal(err)
	}

	return err
}

// create status line with database counts.
func StatusLine() string {

	var countFindings, countNotes int64

	DB.Model(&Finding{}).Count(&countFindings)
	DB.Model(&Note{}).Count(&countNotes)

	fmt.Printf("Findings: %d, Notes: %d \n",
		countFindings,
		countNotes,
	)

	oldest := Finding{}
	newest := Finding{}

	oldest.Oldest()
	newest.Last()

	msg := fmt.Sprintf("oldest: %s newest %s  s\n",
		oldest.CreatedAt,
		newest.CreatedAt,
	)

	return msg
}
