package models

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/Attumm/settingo/settingo"
	"github.com/go-gormigrate/gormigrate/v2"
	"github.com/twpayne/go-geom"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Finding struct {
	gorm.Model

	// Geom is `custom` geometry field(s).
	// Geom is used by the API and Form input.
	// Point, Polygon and Linestring are used to
	// store specific types.

	Geom       *Geometry `json:"geojson" form:"geojson"`
	Point      *Geometry `json:"-"`
	Polygon    *Geometry `json:"-"`
	LineString *Geometry `json:"-"`

	// Owner notes
	UserID       string `json:"userId"`
	UserName     string `json:"username"`
	Reporter     string `json:"reporter"`
	Organization string `json:"organization"`
	Category     string `json:"category"`

	// location object?
	GeoData datatypes.JSON `json:"geodata" form:"geoData"`

	AddresDisplay      string `json:"addressDisplay" form:"addressDisplay"`
	PandID             string `json:"pandId" form:"pandId" gorm:"index"`
	NummeraanduidingID string `json:"nummeraanduidingId" form:"nummeraanduidingId"`
	Street             string `json:"street"`
	StreetNumber       int    `json:"streetNumber" form:"streetNumber"`
	AdditionLetter     string `json:"additionLetter" form:"additionLetter"`
	Addition           string `json:"addition" form:"addition"`
	PostalCode         string `json:"postalCode" gorm:"index"`
	Municipality       string `json:"municipality"`

	Details datatypes.JSON `json:"details"`

	Title       string `json:"title"`
	Description string `json:"description" form:"description"`
}

func NewFinding(g geom.T) (finding Finding, err error) {

	finding = Finding{}
	gc := geom.NewGeometryCollection()

	if err := gc.Push(g); err != nil {
		return finding, err
	}

	finding.Geom = &Geometry{Geom: gc}

	now := time.Now()
	past := now.Add(time.Hour * time.Duration(int64(rand.Float64()*-365)))
	finding.UpdatedAt = past

	return finding, nil
}

func MakeRandomFindings(n int) {

	for i := 0; i < n; i++ {

		finding, err := NewFinding(randomNLPoint())

		if err != nil {
			log.Fatal(err)
		}

		if err = DB.Create(&finding).Error; err != nil {
			log.Fatal(err)
		}

		if err != nil {
			log.Fatal(err)
		}

		finding, err = NewFinding(randomNLPolygon())

		if err != nil {
			log.Fatal(err)
		}

		if err := DB.Create(&finding).Error; err != nil {
			log.Fatal(err)
		}

		if i%100 == 0 {
			log.Printf("%d polygons created\n", i)
		}
	}
}

func (f *Finding) setGeometryCollection() error {
	gc := geom.NewGeometryCollection()
	gg := f.Geom.Geom

	if err := gc.Push(gg); err != nil {
		return err
	}

	f.Geom.Geom = gc

	return nil
}

func (f *Finding) BeforeSave(tx *gorm.DB) error {

	if tx == nil {
		return errors.New("database setup wrong")
	}

	if f.Geom == nil || f.Geom.Geom == nil {
		return errors.New("geometry missing")
	}

	if f.Geom != nil {
		return f.parseGeojson()
	}

	return nil
}

// parseGeojson validates and stores
func (f *Finding) parseGeojson() error {

	var err error

	g := f.Geom.Geom

	switch gg := g.(type) {
	case *geom.Point:
		f.Point = &Geometry{Geom: gg}
		err = f.setGeometryCollection()
	case *geom.Polygon:
		f.Polygon = &Geometry{Geom: g}
		err = f.setGeometryCollection()
	case *geom.LineString:
		f.LineString = &Geometry{Geom: gg}
		err = f.setGeometryCollection()
	case *geom.GeometryCollection:
		for i := range gg.Geoms() {
			switch gg.Geom(i).(type) {
			case *geom.Point:
				f.Point = &Geometry{Geom: gg.Geom(i)}
			case *geom.Polygon:
				f.Polygon = &Geometry{Geom: gg.Geom(i)}
			case *geom.LineString:
				f.LineString = &Geometry{Geom: gg.Geom(i)}
			}
		}
	default:
		return fmt.Errorf("unsupported geojson data type %v", gg)
	}

	if err != nil {
		return fmt.Errorf("woops %s %v", err, f.Geom.Geom)
	}

	return err
}

func (f *Finding) TableName() string {
	return "dook.findings"
}

func (f *Finding) Last() {
	DB.Order("created_at desc").First(f)
}

func (f *Finding) Oldest() {
	DB.Order("created_at asc").First(f)
}

type Note struct {
	gorm.Model
	FindingID string `json:"finding_id" form:"finding_id" gorm:"index"`

	Title       string
	Description string `json:"description" form:"description"`

	// Owner notes
	UserID   string `json:"userId"`
	UserName string `json:"username"`

	Reporter string `json:"reported" form:"reporter"`
	Category string `json:"category" form:"category"`

	Image string

	Details datatypes.JSON
}

func (r *Note) TableName() string {
	return "dook.notes"
}

func runStatements(sqlStatements []string) error {
	for _, stm := range sqlStatements {
		err := DB.Exec(stm).Error
		if err != nil {
			return err
		}
	}

	return nil
}

func setupViews() error {
	sqlStatements := []string{
		`drop view if exists dook.finding_polygon`,
		`create or replace view dook.finding_polygon as (
		select  id, title, user_id,
			user_name,
			extract(epoch from f.updated_at) update_epoch,
			category, organization,
			f.polygon as geom
		from dook.findings f
		where f.polygon is not null and deleted_at is null)`,

		`drop view if exists dook.finding_line`,
		`create or replace view dook.finding_line as (
		select  id, title, user_id,
			user_name,
			extract(epoch from f.updated_at) update_epoch,
			category, organization,
		f.line_string as geom from dook.findings f
		where f.line_string is not null and deleted_at is null)`,

		`drop view if exists dook.finding_point`,
		`create view dook.finding_point as (
		select  id, title, user_id,
			user_name,
			extract(epoch from f.updated_at) update_epoch,
			category, organization,
		f.point as geom from dook.findings f
		where f.point is not null and deleted_at is null)`,
	}

	return runStatements(sqlStatements)
}

func setupFindings() error {
	sqlStatements := []string{
		`CREATE TABLE IF NOT EXISTS dook.findings (
				id SERIAL PRIMARY KEY,
				geom geometry(GEOMETRYCOLLECTION, 4326) NOT NULL,
				point geometry(POINT, 4326),
				polygon geometry(POLYGON, 4326),
				line_string geometry(LINESTRING, 4326)
			);`,
		`CREATE INDEX on dook.findings using gist (geom);`,
		`CREATE INDEX on dook.findings using gist (point);`,
		`CREATE INDEX on dook.findings using gist (polygon);`,
		`CREATE INDEX on dook.findings using gist (line_string);`,
		`ALTER TABLE IF EXISTS dook.findings ALTER COLUMN "id" TYPE bigint`,
	}

	return runStatements(sqlStatements)
}

func customMigrateTables() error {

	sqlStatements := []string{
		`CREATE SCHEMA IF NOT EXISTS dook`,
		`CREATE EXTENSION IF NOT EXISTS postgis`,
	}

	if err := runStatements(sqlStatements); err != nil {
		return err
	}

	opt := gormigrate.DefaultOptions
	opt.TableName = "dook.migrations"

	m := gormigrate.New(DB, opt, []*gormigrate.Migration{
		{
			ID: "init geo",
			Migrate: func(tx *gorm.DB) error {
				return setupFindings()
			},
		},
		{
			ID: "findings_except_geom",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&Finding{})
			},
		},
		{
			ID: "findings_views",
			Migrate: func(tx *gorm.DB) error {
				return setupViews()
			},
		},

		{
			ID: "init note",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&Note{})
			},
		},
	})

	if err := m.Migrate(); err != nil {
		log.Fatalf("could not migrate: %v", err)
		return err
	}

	DB.AutoMigrate(&Finding{})
	DB.AutoMigrate(&Note{})
	err := setupViews()

	if err != nil {
		log.Fatal(err)
	}

	log.Println("done migrating")

	return nil
}

// truncateTables clear tables for test
func TruncateTables() {

	if settingo.Get("POSTGRES_DB") != "test" {
		log.Fatal("trying to tuncate non test database")
		return
	}

	sqlStatements := []string{
		`TRUNCATE TABLE dook.findings`,
		`TRUNCATE TABLE dook.notes`,
	}

	for _, stm := range sqlStatements {
		err := DB.Exec(stm).Error
		if err != nil {
			log.Fatal(err)
		}
	}
}

func randomNLPoint() *geom.Point {
	xr := []float64{3.448706, 7.132251}
	yr := []float64{51.087758, 53.028353}

	x := rand.Float64()*(xr[1]-xr[0]) + xr[0]
	y := rand.Float64()*(yr[1]-yr[0]) + yr[0]

	p := geom.NewPoint(geom.XY)
	p, err := p.SetCoords([]float64{x, y})

	if err != nil {
		log.Fatal(err)
	}

	return p
}

func randomNLPolygon() *geom.Polygon {
	/*
		[
		    [4.905321, 52.377706],
		    [4.90527, 52.377706],
		    [4.90527, 52.377869],
		    [4.905321, 52.377869],
		    [4.905321, 52.377706]
		]
	*/
	p := randomNLPoint()
	x := p.X()
	y := p.Y()

	coordinates := [][]geom.Coord{
		[]geom.Coord{
			{x, y},
			{x - 0.00005, y},
			{x - 0.00005, y + 0.0013},
			{x, y + 0.0013},
			{x, y}},
	}

	poli := geom.NewPolygon(geom.XY)

	_, _ = poli.SetCoords(coordinates)

	return poli
}
