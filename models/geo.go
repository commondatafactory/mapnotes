package models

import (
	"database/sql/driver"

	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/ewkbhex"
	"github.com/twpayne/go-geom/encoding/geojson"
)

// Geometry is a custom postgis geo type to use with Gorm
// to store and save geojson
type Geometry struct {
	Geom geom.T `json:"-"`
}

func (g Geometry) GormDataType() string {
	return "geometry"
}

// Value converts the given Geometry struct into WKT such that it can be stored in a
// database. Implements Valuer interface for use with database operations.
func (g Geometry) Value() (driver.Value, error) {

	_, err := geom.SetSRID(g.Geom, 4326)

	if err != nil {
		return nil, err
	}

	out, err := ewkbhex.Encode(g.Geom, ewkbhex.NDR)

	if err != nil {
		return nil, err
	}

	// stored into DB.
	return out, nil
}

// Scan converts the hexadecimal representation of geometry into the given Geometry
// struct. Implements Scanner interface for use with database operations.
func (g *Geometry) Scan(input interface{}) error {
	gt, err := ewkbhex.Decode(input.(string))

	if err != nil {
		return err
	}

	g.Geom = gt
	return nil
}

func (g *Geometry) MarshalJSON() ([]byte, error) {
	return geojson.Marshal(g.Geom)
}

func (g *Geometry) UnmarshalJSON(b []byte) error {
	return geojson.Unmarshal(b, &g.Geom)
}

// UnmarshalText to convert form text to geojson.
func (g *Geometry) UnmarshalText(text []byte) error {
	return g.UnmarshalJSON(text)
}

func (g *Geometry) MarshalText() ([]byte, error) {
	return geojson.Marshal(g.Geom)
}
